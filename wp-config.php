<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lara_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aB]!$0Mov[F/9WTb>JPban^=<pEB!q)R!ff6d=dlA}y7:fo-##5CiQdwXFywdc[X');
define('SECURE_AUTH_KEY',  '8Qy%)sa3ZaLz1nXGy =jE`9Nteg<J{j+(o]z Gj O&m&j<2b/R~{/sV|`s1`BmRO');
define('LOGGED_IN_KEY',    'rnI&|?,eb1NBzdoN@g3WV:nW8`,#3+tR(0WwaO-P01+#]ZLS0nR+A7+V~0]M!NNt');
define('NONCE_KEY',        '=B}E9cYw.PFkgG}}nlRs #-hEG6LI#p`TD0 i2SD5oNwBoWc%CgI34Z>R:kr>`/1');
define('AUTH_SALT',        '3&+xp=A+o&r%|y;L@(P3P%@?r[`5n}r,_9,US xZu:RX?D<tF,&:=Z~b+{YXuE_<');
define('SECURE_AUTH_SALT', '3SZIY$!-zJWaJV6/?7YC<@vxnXqkxy2;MMg16E5P~pm1Re+`vz_}1tc$/],x;lzJ');
define('LOGGED_IN_SALT',   'P!4,v XKBD9/ijk6q-Z3[j{^so+K@6Pk[L.1Yxo%g.s74AQf+}uf^ep&iDb&:Kre');
define('NONCE_SALT',       'AetGlKHoLv@WrH,O@R3g` yOy&[~QDDo_)Dp$n57 9m@f<H4NhTP-Z3t#)AujxDL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
